#!/usr/bin/env python3.5
from PIL import Image, ImageDraw, ImageFont
import sys
import random
import time
import os

dir_path = os.path.dirname(os.path.realpath(__file__)) + '/'

apinat = ['apina1.jpeg', 'apina2.jpg', 'apina3.jpg', 'apina4.jpg', 'apina5.jpg', 'apina7.jpg', 'apina8.jpg', 'apina9.jpg']
apina_dimensions = [[200, 50], [46, 631], [16, 487], [12, 52], [30, 1286], [30, 710], [25, 547], [27, 632]]

apina = random.randint(0, len(apinat)) - 1

image = Image.open(dir_path + apinat[apina])

draw = ImageDraw.Draw(image)

font = ImageFont.truetype(dir_path + 'RobotoCondensed-BoldItalic.ttf', size=45)

# tekstin positio
(x, y) = (apina_dimensions[apina][0], apina_dimensions[apina][1])

message = 'gief argument' # max 64
if len(sys.argv) == 2:
    message = sys.argv[1]

if (len(message) > 67):
    message = message[:64] + "..."

# print(len(message))
color = 'rgb(0, 0, 0)' # musta

draw.text((x, y), message, fill=color, font=font)
(x, y) = (150, 150)

filename = 'apina' + str(int(time.time())) + '.png'

image.save('/var/www/html/tmp/jamesi/' + filename)
print(filename)
